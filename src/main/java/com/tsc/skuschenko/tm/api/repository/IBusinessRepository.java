package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.api.IRepository;
import com.tsc.skuschenko.tm.model.AbstractBusinessEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity>
        extends IRepository<E> {

    void clear(@NotNull String userId);

    @Nullable
    List<E> findAll(@NotNull String userId);

    @Nullable
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @Nullable
    E findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    E findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    E findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    E removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    E removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    E removeOneByName(@NotNull String userId, @NotNull String name);

}
