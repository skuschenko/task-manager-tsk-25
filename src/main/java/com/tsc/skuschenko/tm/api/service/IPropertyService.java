package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.api.other.ISaltSetting;
import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthor();

    @NotNull
    String getAuthorEmail();

}
