package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public final class VersionShowCommand extends AbstractCommand {

    @NotNull
    private final String ARGUMENT = "-v";

    @NotNull
    private final String DESCRIPTION = "version";

    @NotNull
    private final String NAME = "version";

    @NotNull
    @Override
    public String arg() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        @NotNull final IPropertyService service =
                serviceLocator.getPropertyService();
        System.out.println(service.getApplicationVersion());
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}