package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public final class AboutShowCommand extends AbstractCommand {

    @NotNull
    private final String ARGUMENT = "-a";

    @NotNull
    private final String DESCRIPTION = "about";

    @NotNull
    private final String NAME = "about";

    @NotNull
    @Override
    public String arg() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        @NotNull final IPropertyService service =
                serviceLocator.getPropertyService();
        System.out.println("AUTHOR: " + service.getAuthor());
        System.out.println("EMAIL: " + service.getAuthorEmail());
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
