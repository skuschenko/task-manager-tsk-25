package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    private final String DESCRIPTION = "user login";

    @NotNull
    private final String NAME = "login";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("login");
        @NotNull final String login = TerminalUtil.nextLine();
        showParameterInfo("password");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
